class GoogleMaps {
    constructor() {
        this.searched = false;
        this.currentEfficiency = "duration"
        this.modeDeplacement = {
            DRIVING: {
                styles: {
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'drive.png'
            },
            WALKING: {
                styles: {
                    strokeColor: '#00FF00',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'walk.png'
            },
            BICYCLING: {
                styles: {
                    strokeColor: '#0000FF',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'bicycle.png'
            },
            TRANSIT: {
                styles: {
                    strokeColor: '#FF9000',
                    strokeOpacity: 0.4,
                    strokeWeight: 10
                },
                img: 'transit.png'
            }
        };
        this.efficiency = {
            DURATION: {
                img: 'duration.png'
            },
            DISTANCE: {
                img: 'distance.png'
            }
        }
        this.DOMMap = {
            map: document.getElementById('map'),
            directionsPanel: document.getElementById("directionsPanel"),
            modeDeplacement: document.getElementById("modeTransport"),
            typeEfficiency: document.getElementById("typeEfficiency")
        }
    }

      showMarkers(map,liste, gmarkers) {

        this.setMapOnAll(map, ListeArretBus, gmarkers);
    }
    clearMarkers(map, liste, gmarkers) {

        this.setMapOnAll(null, null, gmarkers);
    }

    setMapOnAll(map, liste, gmarkers) {
        if (liste != null && liste.length > 0)
        {
            liste.forEach((value) => {
                const myLatLng = new google.maps.LatLng(value.stop_lat, value.stop_lon);
                let marker = new google.maps.Marker({
                    position: myLatLng,
                    title: value.stop_name,
                    icon: '/imgs/icons/bus_stop_icon.png'

                });
                marker.setMap(this.map);
                gmarkers.push(marker);
            });
        }
        if (gmarkers) {
            if (liste == null && gmarkers.length > 0) {
                for (let i = 0; i < gmarkers.length; i++) {
                    gmarkers[i].setMap(null);
                }
            }
        }

//            this.gMapMarkers.forEach(marker => marker.setMap(map));

    }
    showOrHideMarkers(map, liste, gmarkers) {
        if (map) {
            if (map.getZoom() >= 16) {
                this.showMarkers(map, liste, gmarkers);
            }
            if (map.getZoom() < 16) {
                this.clearMarkers(null, null,gmarkers);
            }
        }
    }

    initialize() {
        this.directionsService = new google.maps.DirectionsService();
        var lnt = new google.maps.LatLng(45.750000, 4.85);
        var gmarkers = [];
        var options = {
            zoom: 5,
            center: lnt,
            diagId: google.maps.MapTypeId.ROADMAP
        }
        this.map = new google.maps.Map(this.DOMMap.map, options);
         this.map.addListener('zoom_changed', () => {
            this.showOrHideMarkers(this.map, this.ListArretBus, gmarkers);
        });


        for (const [mode, _] of Object.entries(this.modeDeplacement)) {
            this.addModeTransport(mode);
        }

        for (const [type, _] of Object.entries(this.efficiency)) {
            this.addEfficiency(type);
        }
        this.setModeSelected(this.currentEfficiency, "EFFICIENCY");
        this.showOrHideMarkers(this.map);
    }

    parseJson(json, js) {
        let bounds = new google.maps.LatLngBounds();
        for (const [key, value] of Object.entries(json.routes[0].bounds)) {
            bounds.extend(new google.maps.LatLng(value.lat, value.lng));
        }
        json.routes[0].bounds = bounds;

        for (const [key, value] of Object.entries(json.routes[0].legs[0].steps)) {
            let lngEnd = new google.maps.LatLng(parseFloat(json.routes[0].legs[0].steps[key].end_location.lat.toString().replace(",", ".")), parseFloat(json.routes[0].legs[0].steps[key].end_location.lng.toString().replace(",", ".")));
            json.routes[0].legs[0].steps[key].end_location = {
                lat: parseFloat(lngEnd.lat),
                lng: parseFloat(lngEnd.lng)
            };
            json.routes[0].legs[0].steps[key].end_point = json.routes[0].legs[0].steps[key].end_location;

            let lngStart = new google.maps.LatLng(parseFloat(json.routes[0].legs[0].steps[key].start_location.lat.toString().replace(",", ".")), parseFloat(json.routes[0].legs[0].steps[key].start_location.lng.toString().replace(",", ".")));
            json.routes[0].legs[0].steps[key].start_location = {
                lat: parseFloat(lngStart.lat),
                lng: parseFloat(lngStart.lng)
            };
            json.routes[0].legs[0].steps[key].start_point = json.routes[0].legs[0].steps[key].start_location;

            json.routes[0].legs[0].steps[key].instructions = json.routes[0].legs[0].steps[key].html_instructions;
            json.routes[0].legs[0].steps[key].encoded_lat_lngs = json.routes[0].legs[0].steps[key].polyline.points;
            json.routes[0].legs[0].steps[key].path = js.routes[0].legs[0].steps[key].path;
            json.routes[0].overview_path = js.routes[0].overview_path;
            if (json.routes[0].legs[0].steps[key].transit_details !== undefined) {
                json.routes[0].legs[0].steps[key].end_location = js.routes[0].legs[0].steps[key].end_location;
                json.routes[0].legs[0].steps[key].end_point = js.routes[0].legs[0].steps[key].end_point;
                json.routes[0].legs[0].steps[key].start_location = js.routes[0].legs[0].steps[key].start_location;
                json.routes[0].legs[0].steps[key].start_point = js.routes[0].legs[0].steps[key].start_point;
                json.routes[0].legs[0].steps[key].transit = json.routes[0].legs[0].steps[key].transit_details;            }
        }

        return json;
    }
    

    iteneraire(origin, destination) {
        if (this.routes !== undefined) {
            for (const [index, _] of Object.entries(this.routes)) {
                this.routes[index].direction.setMap(null);
                this.routes[index].direction.setPanel(null);
            }
        }
        this.routes = [];
        let modeDeplacementList = Object.entries(this.modeDeplacement);
        let modeDeplacementCount = modeDeplacementList.length;
        let currentCount = 0;
        let modeError = [];
        for (const [mode, _] of modeDeplacementList) {
            let jsResult;
            this.directionsService.route({ origin: origin, destination: destination, travelMode: google.maps.TravelMode[mode] }, (result, status) => {
                jsResult = result;
                fetch(baseURL + "/api/map/itineraire/" + origin + '/' + destination + '/' + google.maps.TravelMode[mode])
                    .then(response => response.json())
                    .then(response => {
                        let json = JSON.parse(response.result.rawJson);
                        if (json.status == google.maps.DirectionsStatus.OK) {
                            json.request = {
                                destination: {
                                    query: destination
                                },
                                origin: {
                                    query: origin
                                },
                                travelMode: mode
                            };
                            json = this.parseJson(json, jsResult);
                            let direction = new google.maps.DirectionsRenderer();
                            direction.setDirections(json);
                            this.routes.push({ mode: mode, direction: direction });
                            $("#duration_" + mode).text(direction.directions.routes[0].legs[0].duration.text);
                            $("#distance_" + mode).text(direction.directions.routes[0].legs[0].distance.text);
                            currentCount++;
                        } else {
                            currentCount++;
                            modeError.push(mode);
                        }

                        if (currentCount == modeDeplacementCount) {
                            if (this.routes.length == 0) {
                                alert("Cette destination est indisponible.");
                            } else if (modeError.length > 0) {
                                alert("Certains moyens de transport sont indisponibles : " + modeError.join(',') + '.');
                                this.searched = true;
                                this.calculEfficiency();
                            } else {
                                this.searched = true;
                                this.calculEfficiency();
                            }
                        }
                    })
            })
        }
    }

    moreEfficiency() {
        return this.routes.reduce((a, b) => {
            return a.direction.directions.routes[0].legs[0][this.currentEfficiency].value < b.direction.directions.routes[0].legs[0][this.currentEfficiency].value ? a : b;
        });
    }

    calculEfficiency() {
        if (!this.searched) {
            return;
        }
        let efficient = this.moreEfficiency();
        let newStyle = this.modeDeplacement;
        newStyle[efficient.mode].styles.strokeOpacity = 1.0;
        newStyle[efficient.mode].styles.strokeWeight = 12;
        let dest = this.routes.findIndex(function (route) { return route.mode == efficient.mode });
        if (this.routes[dest] != null) {
            this.routes[dest].direction.setOptions({ polylineOptions: newStyle[efficient.mode].styles });
            this.routes[dest].direction.setPanel(this.DOMMap.directionsPanel);
            this.setModeSelected(efficient.mode);
        }
        for (const [index, route] of Object.entries(this.routes)) {
            if (index != dest) {
                this.routes[index].direction.setOptions({ polylineOptions: this.modeDeplacement[route.mode].styles });
                this.routes[index].direction.setPanel(null);
            }
            this.routes[index].direction.setMap(this.map);
        }
    }

    modeAvailable(mode) {
        for (const [index, route] of Object.entries(this.routes)) {
            if (route.mode == mode) {
                return true;
            }
        }
    }

    changeMode(mode) {
        if (!this.modeAvailable(mode)) {
            alert("Ce mode de transport n'est pas disponible pour cette destination.");
            return;
        }
        for (const [index, route] of Object.entries(this.routes)) {
            let newStyle = this.modeDeplacement;
            newStyle[route.mode].styles.strokeOpacity = 0.4;
            newStyle[route.mode].styles.strokeWeight = 10;
            if (this.routes[index].mode == mode) {
                newStyle[mode].styles.strokeOpacity = 1.0;
                newStyle[mode].styles.strokeWeight = 12;
                this.routes[index].direction.setOptions({ polylineOptions: newStyle[mode].styles });
                this.routes[index].direction.setPanel(this.DOMMap.directionsPanel);
                this.setModeSelected(mode);
            } else {
                this.routes[index].direction.setPanel(null);
                this.routes[index].direction.setOptions({ polylineOptions: newStyle[route.mode].styles });
            }
            this.routes[index].direction.setMap(null);
            this.routes[index].direction.setMap(this.map);
        }
    }

    changeEfficiency(efficiency) {
        this.currentEfficiency = efficiency.toLowerCase();
        this.calculEfficiency();
        this.setModeSelected(efficiency, "EFFICIENCY");
    }

    FirstLetterUpper(str) {
        return str.charAt(0).toUpperCase() + str.slice(1)
    }

    addModeTransport(mode) {
        this.DOMMap.modeDeplacement.innerHTML += "<div class='image_container'><img id='mode_transport_" + mode.toLowerCase() + "' style='border: 3px " + this.modeDeplacement[mode].styles.strokeColor + " solid;' src='/imgs/deplacements/" + this.modeDeplacement[mode].img + "' type='" + mode + "' /><span>" + this.FirstLetterUpper(mode.toLowerCase()) + "</span><span id='duration_" + mode + "'></span><span id='distance_" + mode +"'></span></div>";
    }

    addEfficiency(efficiency) {
        this.DOMMap.typeEfficiency.innerHTML += "<div class='image_container'><img id='efficiency_" + efficiency.toLowerCase() + "' src='/imgs/efficiency/" + this.efficiency[efficiency].img + "' type='" + efficiency + "' /<span>" + this.FirstLetterUpper(efficiency.toLowerCase()) + "</span></div>";
    }

    setModeSelected(selectedMode, type = "TRANSPORT") {
        if (type == "TRANSPORT") {
            for (const [mode, _] of Object.entries(this.modeDeplacement)) {
                let value = this.modeDeplacement[mode];
                let dom = document.getElementById("mode_transport_" + mode.toLowerCase());
                let style = "border: 3px " + value.styles.strokeColor + " solid;";
                if (dom.getAttribute('id') == 'mode_transport_' + selectedMode.toLowerCase()) {
                    style += "background-color: " + value.styles.strokeColor + "50;";
                }
                dom.setAttribute("style", style);
            }
        } else if (type == "EFFICIENCY") {
            for (const [mode, _] of Object.entries(this.efficiency)) {
                let dom = document.getElementById("efficiency_" + mode.toLowerCase());
                let style;
                if (mode.toLowerCase() == this.currentEfficiency) {
                    style = 'border: 3px purple solid; background-color: #80008050;';
                } else {
                    style = 'border: 0; background-color: transparent;';
                }
                dom.setAttribute('style', style);
            }
        }
    }
}
