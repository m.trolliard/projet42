﻿mapboxgl.accessToken = 'pk.eyJ1IjoiaW1pcm9zbGF2aSIsImEiOiJja3FvNmp1czAwMHRwMnVzNWNscWF4MWlvIn0.hF6DIntxDBZcmGIe05sPzA';
L.mapbox.accessToken = mapboxgl.accessToken;

var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [4.85, 45.750000],
    zoom: 12,
    draggable: false
});

map.addControl(
    new MapboxDirections({
        accessToken: mapboxgl.accessToken
    }),
    'top-left'
);
