﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet42.Models
{
    public class BusStops
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string stop_name { get; set; }
        public string stop_desc { get; set; }
        public string stop_lat { get; set; }
        public string stop_lon { get; set; }
        public int location_type { get; set; }

    }
}
