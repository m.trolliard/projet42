﻿using GoogleApi.Entities.Maps.Common;
using GoogleApi.Entities.Maps.Directions.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Projet42.Maps;
using Projet42.Maps.Maps;
using Projet42.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet42.Controllers
{
    [Route("api/map")]
    [ApiController]
    public class MapController : ControllerBase
    {
        private IMap Imap;

        [HttpPost("defaultMap/{map}")]
        public ActionResult<Boolean> editDefaultMap(string map)
        {
            Response.Cookies.Append("map", map);
            return true;
        }

        [HttpGet("itineraire/{origin}/{destination}/{travelMode}")]
        public ActionResult<MapResult> itineraire(string origin, string destination, string travelMode)
        {
            String map = Request.Cookies["map"];
            if (map == "OpenStreetMap")
            {
                this.Imap = new OpenStreetMap();
            } else
            {
                this.Imap = new GoogleMaps();
            }

            return Imap.iteneraire(origin, destination, travelMode);
        }
    }
}
