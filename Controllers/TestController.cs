﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projet42.Services;

namespace Projet42.Controllers
{
    public class TestController : Controller
    {
        private readonly BusService _busService;
        public TestController(BusService busService)
        {
            _busService = busService;
        }

        private string baseURL { get; set; }

        private void Init()
        {
            baseURL = (Request.IsHttps ? "https://" : "http://") + Request.Host;
        }
        public IActionResult Index()
        {
            var bus = _busService.Get();
            ViewData["Bus"] = bus;
            return View(bus);
        }

        public IActionResult googlemaps()
        {
            Init();
            var bus = _busService.Get();
            ViewData["Bus"] = bus;
            ViewData["BaseURL"] = baseURL;
            return View(bus);
        }

        public IActionResult openstreetmap()
        {
            Init();
            ViewData["BaseURL"] = baseURL;
            return View();
        }
    }
}
