﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using Projet42.Models;

namespace Projet42.Services
{
    public class BusService
    {
        private readonly IMongoCollection<BusStops> _Busstops;

        public BusService(IBusDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _Busstops = database.GetCollection<BusStops>(settings.BusCollectionName);
        }

        public List<BusStops> Get() =>
            _Busstops.Find(bus => true).ToList();
        public BusStops Get(string id) =>
            _Busstops.Find<BusStops>(bus => bus.Id == id).FirstOrDefault();

        public BusStops Create(BusStops bus)
        {
            _Busstops.InsertOne(bus);
            return bus;
        }
        public void Update(string id, BusStops busIn) =>
            _Busstops.ReplaceOne(bus => bus.Id == id, busIn);

        public void Remove(BusStops busIn) =>
            _Busstops.DeleteOne(bus => bus.Id == busIn.Id);

        public void Remove(string id) =>
            _Busstops.DeleteOne(bus => bus.Id == id);
    
}

}
