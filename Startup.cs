using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Projet42.Models;
using Projet42.RabbitMQ;
using Projet42.Services;
using Projet42.Tasks;
using Projet42.Tasks.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projet42
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            TasksManager.Start();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<BusDatabaseSettings>(
            Configuration.GetSection(nameof(BusDatabaseSettings)));

            services.AddSingleton<IBusDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<BusDatabaseSettings>>().Value);

            services.AddControllers();
            services.AddSingleton<BusService>();
            services.AddControllersWithViews();
            services.AddSingleton<rabbitMQ>(new rabbitMQ());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
