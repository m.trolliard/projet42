﻿using GoogleApi.Entities.Maps.Common;
using GoogleApi.Entities.Maps.Directions.Request;
using GoogleApi.Entities.Maps.Directions.Response;
using Newtonsoft.Json;
using Projet42.RabbitMQ;
using Projet42.Models;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet42.Maps.Maps
{
    public class GoogleMaps : IMap
    {
        private String TOKEN = "AIzaSyBI9Ha6U7b2qNYHDs3NqtfH9sXMy4LjHI8";

        private rabbitMQ RabbitMQ = new rabbitMQ();

        public MapResult iteneraire(string origin, string destination, string travelMode)
        {
            DirectionsRequest request = new DirectionsRequest();
            String travel = travelMode.ToLower();

            request.Key = this.TOKEN;
            request.Origin = new Location(origin);
            request.Destination = new Location(destination);
            request.TravelMode = (GoogleApi.Entities.Maps.Common.Enums.TravelMode)Enum.Parse(typeof(GoogleApi.Entities.Maps.Common.Enums.TravelMode), char.ToUpper(travel[0]) + travel.Substring(1)); 

            MapResult mapResult = new MapResult();
            DirectionsResponse result = GoogleApi.GoogleMaps.Directions.Query(request);
            mapResult.result = GoogleApi.GoogleMaps.Directions.Query(request);
            this.RabbitMQ.Publish(new { Origine = origin, Destination = destination, ModeTransport = travelMode });
            return mapResult;
        }
    }
}
