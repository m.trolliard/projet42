﻿using GoogleApi.Entities;
using Projet42.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet42.Maps
{
    interface IMap
    {
        public MapResult iteneraire(string origin, string destination, string travelMode);
    }
}
