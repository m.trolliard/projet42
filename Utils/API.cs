﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Projet42.Models;

namespace Projet42.Utils
{
    public class API
    {
        private static readonly HttpClient client = new HttpClient();
        public static async Task<T> call<T>(string url)
        {
            var streamTask = client.GetStreamAsync(url);
            var result = await JsonSerializer.DeserializeAsync<T>(await streamTask);
            return result;
        }
    }
}
