using MongoDB.Bson;
using MongoDB.Driver;
using Projet42.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Projet42.Tasks.Jobs
{
    public class Bus : IJob
    {
        Task IJob.Execute(IJobExecutionContext context)
        {
            var client = new MongoClient("mongodb://192.168.42.24:49153/?readPreference=primary&appname=MongoDB%20Compass&ssl=false");
            var database = client.GetDatabase("Projet42");
            var collec = database.GetCollection<BsonDocument>("Bus");
            var documents = collec.Find(new BsonDocument()).ToList();
            WebClient webClient = new WebClient();
            webClient.DownloadFile("https://navitia.opendatasoft.com/explore/dataset/fr-se/files/1ca6a582346f846728c1138c92f1de36/download/", @".\fr-se_gtfs.zip");

            string zipPath = @".\fr-se_gtfs.zip";

            //Console.WriteLine("Provide path where to extract the zip file:");
            //string extractPath = Console.ReadLine();


            string extractPath = @"D:\Test";

            bool exists = System.IO.Directory.Exists(extractPath);

            if (!exists)
                System.IO.Directory.CreateDirectory(extractPath);
            extractPath = Path.GetFullPath(extractPath);

            // Ensures that the last character on the extraction path
            // is the directory separator char.
            // Without this, a malicious zip file could try to traverse outside of the expected
            // extraction path.
            if (!extractPath.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal))
                extractPath += Path.DirectorySeparatorChar;

            using (ZipArchive archive = new ZipArchive(File.OpenRead(zipPath), ZipArchiveMode.Read))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {

                    if (entry.FullName.EndsWith(".txt", StringComparison.OrdinalIgnoreCase))
                    {
                        // Gets the full path to ensure that relative segments are removed.
                        string destinationPath = Path.GetFullPath(Path.Combine(extractPath, entry.FullName));
                        if (!File.Exists(destinationPath))
                        {
                            // Ordinal match is safest, case-sensitive volumes can be mounted within volumes that
                            // are case-insensitive.
                            if (destinationPath.StartsWith(extractPath, StringComparison.Ordinal))
                                entry.ExtractToFile(destinationPath);
                        }

                    }
                }
            }

            int id = 1;
            string[] lines = File.ReadAllLines(@"D:\Test\stops.txt");
            var arr = new BsonArray();
            BsonDocument[] bson = new BsonDocument[lines.Length];

            foreach (string str in lines)
            {
                string[] parts = str.Split(',');

                BusStops Bus = new BusStops();
                Bus.stop_name = parts[1];
                Bus.stop_lat = parts[3];
                Bus.stop_lon = parts[4];
                //addRecord(id, parts[1], parts[3], parts[4], @"D:\Test\csv.csv");
                var document = new BsonDocument
                {
                    {"stop_name",Bus.stop_name },
                    {"stop_lat",Bus.stop_lat},
                    {"stop_lon",Bus.stop_lon}
                };
                arr.Add(document);
                bson.Append(document);
                var NameExist = collec.Find(Bus.stop_name);
                collec.Find($"{{ stop_name: Bus.stop_name }}");

                collec.InsertOne(document);


                var filter = Builders<BsonDocument>.Filter.Eq("stop_name", Bus.stop_name);
                var update = Builders<BsonDocument>.Update.Set("stop_lat", "27.27");



                // collec.UpdateOne(filter, update);

                //collec.UpdateOne.upsert()(document,, SafeMode.False);

            }
            foreach (BsonDocument doc in documents)
            {
                Console.WriteLine(doc.ToString());
            }


            return null;
        }
        
        //Console.WriteLine("ARR : " + arr[1]);
        //bson.Add(arr);
        //collec.InsertMany(bson);

    }

        
    
}
