using Microsoft.Extensions.Hosting;
using Projet42.Tasks.Jobs;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Projet42.Tasks
{
    public class TasksManager
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().GetAwaiter().GetResult();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<Bus>().Build();

            var onSunday = DailyTimeIntervalScheduleBuilder.Create()
                         .OnDaysOfTheWeek(new DayOfWeek[] { DayOfWeek.Sunday });

            ITrigger trigger = TriggerBuilder.Create()
                //.StartNow()
                .WithSchedule(onSunday)
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}
